<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ImportDB extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Eloquent::unguard();

        $path = 'database/test.sql';
        \DB::unprepared(file_get_contents($path));
    }
}
