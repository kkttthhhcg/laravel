<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class Test2 extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $a = new \App\Http\Controllers\TestController();
        $response = $a->index();
        $this->assertEquals('abcd', $response);
    }

    public function testIndex2()
    {
        $a = new \App\Http\Controllers\TestController();
        $response = $a->index2();
        $this->assertEquals(1, $response);
    }
}
