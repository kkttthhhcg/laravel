<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TestController extends Controller
{
    public function index()
    {
    	return 'abcd';
    }

    public function index2()
    {
    	$a = 0;
    	$b = 1;
    	$c = $a+$b;
    	return $c;
    }

    public function index3()
    {
        $data =  DB::table('users')->get();
        return ['data' => $data];
    }
}
